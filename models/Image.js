var mongoose = require('mongoose');

var ImageSchema = new mongoose.Schema({
  data: Buffer
});

module.exports = mongoose.model('Image', ImageSchema);
