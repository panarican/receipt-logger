var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
   name: {
      first: {type: String, required: true},
      last: {type: String, required: true},
   },
   birthdate: Date
});

module.exports = mongoose.model('User', UserSchema);
