var mongoose = require('mongoose');

var ReceiptSchema = new mongoose.Schema({
  name: String,
  test: String,
  note: String
});


ReceiptSchema.path('test').required(true, 'grrr :( ');

module.exports = mongoose.model('Receipt', ReceiptSchema);
