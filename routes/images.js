var express = require('express');
var router = express.Router();
var fs = require("fs");
var mkdirp = require('mkdirp');

var mongoose = require('mongoose');
var ImageModel = require('../models/Image.js');


/* POST /todos */
router.post('/', function(req, res, next) {
  var buffer = new Buffer(req.body.data, 'base64');
  var directory = "images/2016/";
  mkdirp(directory, function (err) {
    fs.writeFile(directory + "buffer-test2.png", buffer, function(err){
  		console.log(err);
  	});
  });

  ImageModel.create(req.body, function (err, post) {
    if (err) return next(err);
    //res.json(err);
    res.json(post);
  });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  ImageModel.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  ImageModel.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
